package model;

import java.sql.Date;

public class Comanda {
    private int idComanda;
    private int idClient;
    private int idProdus;
    private Date data;
    private int cantitate;

    public Comanda(int idComanda, int idClient, int idProdus, Date data, int cantitate) {
        this.idComanda = idComanda;
        this.idClient = idClient;
        this.idProdus = idProdus;
        this.data = data;
        this.cantitate = cantitate;
    }

    public int getIdComanda() {
        return idComanda;
    }

    public void setIdComanda(int idComanda) {
        this.idComanda = idComanda;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
}
