package model;

public class Produs {
    private int idProdus;
    private String nume;
    private int pret;
    private int stoc;

    public Produs(int idProdus, String nume, int pret, int stoc) {
        this.idProdus = idProdus;
        this.nume = nume;
        this.pret = pret;
        this.stoc = stoc;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public int getStoc() {
        return stoc;
    }

    public void setStoc(int stoc) {
        this.stoc = stoc;
    }
}
