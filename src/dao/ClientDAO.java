package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {
    private static final String findStatementString = "SELECT * FROM client";
    private static final String insertStatementString = "INSERT INTO client VALUES (?,?,?,?,?)";
    private static final String deleteStatementString = "DELETE FROM client where idClient = ?";
    private static final String updateStatementString = "UPDATE client SET nume=?, adresa=?, telefon=?, email=? WHERE idClient=?";

    public static ArrayList<Client> find() {
        ArrayList<Client> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            rs = findStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("idClient");
                String name = rs.getString("nume");
                String adresa = rs.getString("adresa");
                String telefon = rs.getString("telefon");
                String email = rs.getString("email");
                toReturn.add(new Client(id, name, adresa, telefon, email));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static void insert(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;

        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, client.getIdClient());
            insertStatement.setString(2,client.getNume());
            insertStatement.setString(3,client.getAdresa());
            insertStatement.setString(4,client.getTelefon());
            insertStatement.setString(5,client.getEmail());

            insertStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void delete(int id) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        int insertedId = -1;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);

            deleteStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void update(Client client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        int insertedId = -1;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);
            updateStatement.setString(1,client.getNume());
            updateStatement.setString(2,client.getAdresa());
            updateStatement.setString(3,client.getTelefon());
            updateStatement.setString(4,client.getEmail());
            updateStatement.setInt(5, client.getIdClient());

            updateStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
    }


}
