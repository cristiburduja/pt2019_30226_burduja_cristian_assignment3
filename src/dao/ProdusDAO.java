package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Produs;

public class ProdusDAO {
    private static final String findStatementString = "SELECT * FROM produs";
    private static final String insertStatementString = "INSERT INTO produs VALUES (?,?,?,?)";
    private static final String deleteStatementString = "DELETE FROM produs where idProdus = ?";
    private static final String updateStatementString = "UPDATE produs SET nume=?, pret=?, stoc=? WHERE idProdus=?";

    public static ArrayList<Produs> find() {
        ArrayList<Produs> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            rs = findStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("idProdus");
                String name = rs.getString("nume");
                int pret = rs.getInt("pret");
                int stoc = rs.getInt("stoc");
                toReturn.add(new Produs(id, name, pret, stoc));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static void insert(Produs produs) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, produs.getIdProdus());
            insertStatement.setString(2,produs.getNume());
            insertStatement.setInt(3,produs.getPret());
            insertStatement.setInt(4,produs.getStoc());

            insertStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void delete(int id) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        int insertedId = -1;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);

            deleteStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void update(Produs produs) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        int insertedId = -1;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString);

            updateStatement.setString(1,produs.getNume());
            updateStatement.setInt(2,produs.getPret());
            updateStatement.setInt(3,produs.getStoc());
            updateStatement.setInt(4, produs.getIdProdus());

            updateStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
    }


}
