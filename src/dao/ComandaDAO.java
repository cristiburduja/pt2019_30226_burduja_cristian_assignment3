package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Comanda;

public class ComandaDAO {
    private static final String findStatementString = "SELECT * FROM Comanda";
    private static final String insertStatementString = "INSERT INTO Comanda VALUES (?,?,?,?,?)";

    public static ArrayList<Comanda> find() {
        ArrayList<Comanda> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            rs = findStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("idComanda");
                int idC = rs.getInt("idClient");
                int idP = rs.getInt("idProdus");
                Date data=rs.getDate("data");
                int cantitate = rs.getInt("cantitate");
                toReturn.add(new Comanda(id, idC, idP,data, cantitate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static void insert(Comanda Comanda) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, Comanda.getIdComanda());
            insertStatement.setInt(2,Comanda.getIdClient());
            insertStatement.setInt(3,Comanda.getIdProdus());
            insertStatement.setDate(4,Comanda.getData());
            insertStatement.setInt(5,Comanda.getCantitate());

            insertStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }


}
