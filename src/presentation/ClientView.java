package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ClientView extends JFrame{
    private JPanel panel=new JPanel();
    private JScrollPane scrollPane=new JScrollPane();
    private JButton view=new JButton("View clients");
    private JButton add=new JButton("Add client");
    private JButton delete=new JButton("Delete client");
    private JButton edit=new JButton("Edit client");
    private JTextField id=new JTextField("id",10);
    private JTextField nume=new JTextField("nume",10);
    private JTextField adresa=new JTextField("adresa",10);
    private JTextField telefon=new JTextField("telefon",10);
    private JTextField email=new JTextField("email",10);

    public ClientView(){
        this.setSize(800,800);
        panel.add(id);
        panel.add(nume);
        panel.add(adresa);
        panel.add(telefon);
        panel.add(email);
        panel.add(view);
        panel.add(add);
        panel.add(delete);
        panel.add(edit);
        panel.add(scrollPane);

        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<Object> objects= new ArrayList<>();
                objects.addAll(ClientBLL.findClients());
                panel.remove(scrollPane);
                scrollPane=createTable(objects);
                panel.add(scrollPane);
                panel.revalidate();
            }
        });

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idC=Integer.valueOf(id.getText());
                String numeC=nume.getText();
                String adresaC=adresa.getText();
                String telefonC=telefon.getText();
                String emailC=email.getText();
                Client c=new Client(idC,numeC,adresaC,telefonC,emailC);
                ClientBLL.insert(c);
            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idC=Integer.valueOf(id.getText());
                ClientBLL.delete(idC);
            }
        });

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idC=Integer.valueOf(id.getText());
                String numeC=nume.getText();
                String adresaC=adresa.getText();
                String telefonC=telefon.getText();
                String emailC=email.getText();
                Client c=new Client(idC,numeC,adresaC,telefonC,emailC);
                ClientBLL.update(c);
            }
        });

        this.setContentPane(panel);
        this.setVisible(true);
    }

    public JScrollPane createTable(ArrayList<Object> objects){
        DefaultTableModel tModel=new DefaultTableModel();
        JTable table;
        for(Field f:objects.get(0).getClass().getDeclaredFields()){
            tModel.addColumn(f.getName());
        }
        int i=0;
        for(Object o:objects){
            i=0;
            Object[] values=new Object[o.getClass().getDeclaredFields().length];
            for(Field f:o.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                try {
                    values[i++] = f.get(o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            tModel.addRow(values);
        }
        table=new JTable(tModel);
        return new JScrollPane(table);
    }
}
