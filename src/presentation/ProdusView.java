package presentation;

import bll.ProdusBLL;
import model.Produs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ProdusView extends JFrame{
    private JPanel panel=new JPanel();
    private JScrollPane scrollPane=new JScrollPane();
    private JButton view=new JButton("View produse");
    private JButton add=new JButton("Add produs");
    private JButton delete=new JButton("Delete produs");
    private JButton edit=new JButton("Edit produs");
    private JTextField id=new JTextField("id",10);
    private JTextField nume=new JTextField("nume",10);
    private JTextField pret=new JTextField("pret",10);
    private JTextField stoc=new JTextField("stoc",10);

    public ProdusView(){
        this.setSize(800,800);
        panel.add(id);
        panel.add(nume);
        panel.add(pret);
        panel.add(stoc);
        panel.add(view);
        panel.add(add);
        panel.add(delete);
        panel.add(edit);
        panel.add(scrollPane);

        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<Object> objects= new ArrayList<>();
                objects.addAll(ProdusBLL.findProduss());
                panel.remove(scrollPane);
                scrollPane=createTable(objects);
                panel.add(scrollPane);
                panel.revalidate();
            }
        });

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idP=Integer.valueOf(id.getText());
                String numeP=nume.getText();
                int pretP=Integer.valueOf(pret.getText());
                int stocP=Integer.valueOf(stoc.getText());
                Produs c=new Produs(idP,numeP,pretP,stocP);
                ProdusBLL.insert(c);
            }
        });

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idC=Integer.valueOf(id.getText());
                ProdusBLL.delete(idC);
            }
        });

        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idP=Integer.valueOf(id.getText());
                String numeP=nume.getText();
                int pretP=Integer.valueOf(pret.getText());
                int stocP=Integer.valueOf(stoc.getText());
                Produs c=new Produs(idP,numeP,pretP,stocP);
                ProdusBLL.update(c);
            }
        });

        this.setContentPane(panel);
        this.setVisible(true);
    }

    public JScrollPane createTable(ArrayList<Object> objects){
        DefaultTableModel tModel=new DefaultTableModel();
        JTable table;
        for(Field f:objects.get(0).getClass().getDeclaredFields()){
            tModel.addColumn(f.getName());
        }
        int i=0;
        for(Object o:objects){
            i=0;
            Object[] values=new Object[o.getClass().getDeclaredFields().length];
            for(Field f:o.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                try {
                    values[i++] = f.get(o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            tModel.addRow(values);
        }
        table=new JTable(tModel);
        return new JScrollPane(table);
    }
}
