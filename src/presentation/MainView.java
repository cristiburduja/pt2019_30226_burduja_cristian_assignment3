package presentation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainView extends JFrame {
    private JPanel panel=new JPanel();
    private JButton client=new JButton("Client");
    private JButton produs=new JButton("Produs");
    private JButton comanda=new JButton("Comanda");

    public MainView(){
        this.setSize(800,800);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel.add(client);
        panel.add(comanda);
        panel.add(produs);
        client.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientView c=new ClientView();
            }
        });

        produs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProdusView p=new ProdusView();
            }
        });

        comanda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComandaView c=new ComandaView();
            }
        });

        this.setContentPane(panel);
        this.setVisible(true);
    }


}
