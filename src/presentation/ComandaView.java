package presentation;

import bll.ComandaBLL;
import bll.ProdusBLL;
import model.Comanda;
import model.Produs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;

public class ComandaView extends JFrame{
    private JPanel panel=new JPanel();
    private JScrollPane scrollPane=new JScrollPane();
    private JButton view=new JButton("View Comanda");
    private JButton add=new JButton("Add Comanda");
    private JTextField id=new JTextField("id",10);
    private JTextField client=new JTextField("idClient",10);
    private JTextField produs=new JTextField("idProdus",10);
    private JTextField data=new JTextField("Data",10);
    private JTextField cantitate=new JTextField("Cantitate",10);

    public ComandaView(){
        this.setSize(800,800);
        panel.add(id);
        panel.add(client);
        panel.add(produs);
        panel.add(data);
        panel.add(cantitate);
        panel.add(view);
        panel.add(add);
        panel.add(scrollPane);

        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<Object> objects= new ArrayList<>();
                objects.addAll(ComandaBLL.findComanda());
                panel.remove(scrollPane);
                scrollPane=createTable(objects);
                panel.add(scrollPane);
                panel.revalidate();
            }
        });

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idC=Integer.valueOf(id.getText());
                int idClient=Integer.valueOf(client.getText());
                int idProdus=Integer.valueOf(produs.getText());
                Date dataC=Date.valueOf(data.getText());
                int cant=Integer.valueOf(cantitate.getText());
                ArrayList<Produs> arrayp= ProdusBLL.findProduss();
                Produs prod=null;
                for(Produs p:arrayp){
                    if(p.getIdProdus()==idProdus) {
                        prod = p;
                        break;
                    }
                }
                if(prod.getStoc()<cant){
                    System.out.println("Stoc insuficient pentru produsul ales");
                }
                else {
                    prod.setStoc(prod.getStoc()-cant);
                    ProdusBLL.update(prod);
                    Comanda c = new Comanda(idC, idClient, idProdus, dataC, cant);
                    ComandaBLL.insert(c);
                }
            }
        });


        this.setContentPane(panel);
        this.setVisible(true);
    }

    public JScrollPane createTable(ArrayList<Object> objects){
        DefaultTableModel tModel=new DefaultTableModel();
        JTable table;
        for(Field f:objects.get(0).getClass().getDeclaredFields()){
            tModel.addColumn(f.getName());
        }
        int i=0;
        for(Object o:objects){
            i=0;
            Object[] values=new Object[o.getClass().getDeclaredFields().length];
            for(Field f:o.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                try {
                    values[i++] = f.get(o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            tModel.addRow(values);
        }
        table=new JTable(tModel);
        return new JScrollPane(table);
    }
}
