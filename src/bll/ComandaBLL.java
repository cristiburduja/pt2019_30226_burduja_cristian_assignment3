package bll;

import dao.ComandaDAO;
import model.Comanda;

import java.util.ArrayList;

public class ComandaBLL {

    public static ArrayList<Comanda> findComanda(){
        ArrayList<Comanda> comanda= ComandaDAO.find();
        return comanda;
    }

    public static void insert(Comanda comanda){
        ComandaDAO.insert(comanda);
    }
}
