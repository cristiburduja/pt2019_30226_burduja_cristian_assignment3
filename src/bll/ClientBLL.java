package bll;


import dao.ClientDAO;
import model.Client;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientBLL {


    public static ArrayList<Client> findClients(){
        ArrayList<Client> clients= ClientDAO.find();
        return clients;
    }

    public static void insert(Client client){
        ClientDAO.insert(client);
    }

    public static void delete(int id) {
        ClientDAO.delete(id);
    }

    public static void update(Client client) {
        ClientDAO.update(client);
    }
}
