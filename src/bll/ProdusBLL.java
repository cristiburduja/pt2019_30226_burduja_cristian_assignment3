package bll;

import dao.ProdusDAO;
import model.Produs;

import java.util.ArrayList;

public class ProdusBLL {

    public static ArrayList<Produs> findProduss(){
        ArrayList<Produs> produs= ProdusDAO.find();
        return produs;
    }

    public static void insert(Produs produs){
        ProdusDAO.insert(produs);
    }

    public static void delete(int id) {
        ProdusDAO.delete(id);
    }

    public static void update(Produs produs) {
        ProdusDAO.update(produs);
    }
}
